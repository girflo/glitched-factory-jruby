# frozen_string_literal: true

class GlitchedFactory
  module View
    # View for the sampler tab
    class Sampler
      # Settings for the sampler tab
      class SamplerData
        attr_accessor :included_algo

        def initialize
          self.included_algo = included_algo_options
        end

        def included_algo_options
          ShatteredMachine::Sampler::ALL_ALGORITHMS
        end
      end

      include Glimmer::UI::CustomWidget

      before_body do
        @data = SamplerData.new
      end

      body do
        @io = io_fields_single
        group do
          text CONTENT['sampler']['options']
          font height: 16, style: :bold
          layout_data :fill, :fill, true, true

          label do
            text CONTENT['sampler']['included_algorithms']
            font height: 16
          end

          checkbox_group do
            selection bind(@data, :included_algo)
          end
        end

        button do
          text CONTENT['sampler']['button']

          on_widget_selected do
            action = proc do
              ShatteredMachine::Sampler.new(@io.object,
                                            { algorithms_to_sample: @data.included_algo })
                                       .call
            end
            Queue.conveyor.add_parcel(CONTENT['sampler']['title'], '', action)
            Queue.conveyor.start_belt
          end
        end
      end
    end
  end
end

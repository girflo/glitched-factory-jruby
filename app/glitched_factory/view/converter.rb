# frozen_string_literal: true

require 'shattered_machine'

class GlitchedFactory
  module View
    # View for the converter tab
    class Converter
      include Glimmer::UI::CustomWidget

      body do
        @io = io_fields
        @conveyor = GlitchedFactory::Queue.conveyor

        button do
          text CONTENT['converter']['button']

          on_widget_selected do
            @conveyor.add_parcel(CONTENT['converter']['title'], '',
                                 GlitchedFactory::Converter.new(@io.object).call)
          end
        end
      end
    end
  end
end

# frozen_string_literal: true

require_relative '../model/helper'
require_relative '../model/queue'
require_relative 'converter'
require_relative 'documentation'
require_relative 'glitcher/glitcher'
require_relative 'glitcher/brush'
require_relative 'glitcher/change_byte'
require_relative 'glitcher/defect'
require_relative 'glitcher/exchange'
require_relative 'glitcher/pixel_sort'
require_relative 'glitcher/slim'
require_relative 'glitcher/transpose'
require_relative 'glitcher/wrong_filter'
require_relative 'io_fields/io_fields'
require_relative 'io_fields/io_fields_single'
require_relative 'io_fields/input_field'
require_relative 'io_fields/input_field_single'
require_relative 'io_fields/output_field'
require_relative 'progressive_sorter'
require_relative 'sampler'

class GlitchedFactory
  module View
    class AppView
      include Glimmer::UI::CustomShell

      ## Use before_body block to pre-initialize variables to use in body
      #
      #
      before_body do
        Display.app_name = CONTENT['app_title']
        Display.app_version = VERSION
        @display = display do
          on_about do
            display_about_dialog
          end

          on_preferences do
            display_preferences_dialog
          end
        end
      end

      ## Add widget content inside custom shell body
      ## Top-most widget must be a shell or another custom shell
      #
      body do
        shell do
          # Replace example content below with custom shell content
          minimum_size 420, 240
          image File.join(APP_ROOT, 'icons', 'windows', 'Glitched Factory.ico') if OS.windows?
          image File.join(APP_ROOT, 'icons', 'linux', 'Glitched Factory.png') unless OS.windows?
          text CONTENT['app_title']

          tab_folder do
            layout_data :fill, :fill, true, true
            tab_item do
              text CONTENT['glitcher']['title']
              glitcher
            end
            tab_item do
              text CONTENT['sampler']['title']
              sampler
            end
            tab_item do
              text CONTENT['progressive_sorting']['title']
              progressive_sorter
            end
            tab_item do
              text CONTENT['converter']['title']
              converter
            end
            tab_item do
              text '?'
              documentation
            end
          end

          menu_bar do
            menu do
              text '&File'

              menu_item do
                text '&About...'

                on_widget_selected do
                  display_about_dialog
                end
              end
            end
          end
        end
      end

      def display_about_dialog
        message_box(body_root) do
          text CONTENT['about']['title']
          message CONTENT['about']['content']
          message "#{CONTENT['app_title']} #{VERSION}\n\n#{LICENSE}"
        end.open
      end
    end
  end
end

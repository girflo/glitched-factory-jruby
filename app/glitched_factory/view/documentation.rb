# frozen_string_literal: true

require 'shattered_machine'

class GlitchedFactory
  module View
    # View for the documentation tab
    class Documentation
      include Glimmer::UI::CustomWidget

      body do
        tab_folder do
          layout_data :fill, :fill, true, true
          %w[glitcher sampler sorter].each do |section|
            tab_item do
              text CONTENT['help'][section]['title']

              scrolled_composite do
                layout_data :fill, :fill, true, true

                composite do
                  %w[goal input output options].each do |subsection|
                    label do
                      text CONTENT['help'][section][subsection]['title']
                      font height: 16, style: :bold
                    end
                    label { text CONTENT['help'][section][subsection]['content'] }
                  end
                end
              end
            end
          end
          tab_item do
            text CONTENT['about']['title']
            label { text CONTENT['about']['content'] }
          end
        end
      end
    end
  end
end

# frozen_string_literal: true

class GlitchedFactory
  module View
    class Glitcher
      # View for the brush settings tab of the glitcher
      class Brush
        include Glimmer::UI::CustomWidget

        # Settings for the brush tab of the glitcher
        class Setting
          attr_accessor :direction, :direction_options, :probability, :min_pixels, :max_pixels

          def initialize
            self.direction_options = ['Horizontal', 'Horizontal inverted', 'Vertical', 'Vertical inverted']
            self.direction = 'Horizontal'
            self.probability = 95
            self.min_pixels = 1
            self.max_pixels = 10
          end
        end

        def formatted_settings
          { direction: Helper.string_to_symbol(@setting.direction),
            probability: @setting.probability.to_s,
            min_pixels: @setting.min_pixels.to_s,
            max_pixels: @setting.max_pixels.to_s }
        end

        before_body do
          @setting = Setting.new
        end

        body do
          composite do
            grid_layout(3, false) do
              margin_width 0
              margin_height 0
            end
            layout_data :fill, :fill, true, false

            composite do
              layout_data :fill, :fill, true, false
              label do
                text 'Direction'
                font height: 16
              end

              combo(:read_only) do
                selection bind(@setting, :direction)
              end
            end

            composite do
              layout_data :fill, :fill, true, false
              label do
                text 'Probability'
                font height: 16
              end

              composite do
                grid_layout(2, false) do
                  margin_width 0
                  margin_height 0
                end
                layout_data :fill, :center, true, false

                scale do
                  minimum 0
                  maximum 100
                  selection <=> [@setting, :probability]
                end

                label do
                  layout_data :right, :center, false, false
                  text <= [@setting, :probability, { on_read: ->(value) { "#{value} " } }]
                  font height: 16
                end
              end
            end

            composite do
              layout_data :fill, :fill, true, false
              label do
                text 'Minimum glitched pixels'
                font height: 16
              end

              spinner do
                digits 0
                minimum 0
                maximum 10_000
                increment 1
                page_increment 10
                selection <=> [@setting, :min_pixels]
              end
            end

            composite do
              layout_data :fill, :fill, true, false
              label do
                text 'Maximum glitched pixels'
                font height: 16
              end

              spinner do
                digits 0
                minimum 1
                maximum 10_000
                increment 1
                page_increment 10
                selection <=> [@setting, :max_pixels]
              end
            end
          end
        end
      end
    end
  end
end

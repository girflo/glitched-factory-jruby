# frozen_string_literal: true

class GlitchedFactory
  module View
    class Glitcher
      # View for the transpose settings tab of the glitcher
      class Transpose
        include Glimmer::UI::CustomWidget

        # Settings for the transpose tab of the glitcher
        class Setting
          attr_accessor :filter, :filter_options, :force, :force_options

          def initialize
            self.filter_options = %w[None Sub Up Average Paeth]
            self.filter = 'Average'
            self.force_options = %w[Half Full]
            self.force = 'Half'
          end
        end

        def formatted_settings
          { filter: @setting.filter.downcase, transpose_force: @setting.force.downcase }
        end

        before_body do
          @setting = Setting.new
        end

        body do
          composite do
            grid_layout(3, false) do
              margin_width 0
              margin_height 0
            end
            layout_data :fill, :fill, true, false

            composite do
              layout_data :fill, :fill, true, false
              label do
                text 'Filter'
                font height: 16
              end

              combo(:read_only) do
                selection bind(@setting, :filter) # also binds to country_options by convention
              end
            end

            composite do
              layout_data :fill, :fill, true, false
              label do
                text 'Force'
                font height: 16
              end

              combo(:read_only) do
                selection bind(@setting, :force) # also binds to country_options by convention
              end
            end
          end
        end
      end
    end
  end
end

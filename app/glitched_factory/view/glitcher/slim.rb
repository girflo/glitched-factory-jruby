# frozen_string_literal: true

class GlitchedFactory
  module View
    class Glitcher
      # View for the slim settings tab of the glitcher
      class Slim
        include Glimmer::UI::CustomWidget

        # Settings for the slim tab of the glitcher
        class Setting
          attr_accessor :direction, :direction_options, :probability, :colors, :colors_options

          ALL_COLORS = %w[white black grey red green blue cyan yellow magenta].freeze

          def initialize
            self.direction_options = ['Horizontal', 'Horizontal inverted', 'Vertical', 'Vertical inverted']
            self.direction = 'Horizontal'
            self.probability = 95
            self.colors_options = ALL_COLORS
            self.colors = ALL_COLORS
          end
        end

        def formatted_settings
          { direction: Helper.string_to_symbol(@setting.direction),
            probability: @setting.probability.to_s,
            probability_area: 'global',
            colors: @setting.colors.map { |c| Helper.string_to_symbol(c) } }
        end

        before_body do
          @setting = Setting.new
        end

        body do
          composite do
            grid_layout(3, false) do
              margin_width 0
              margin_height 0
            end
            layout_data :fill, :fill, true, false

            composite do
              layout_data :fill, :fill, true, false
              label do
                text 'Direction'
                font height: 16
              end

              combo(:read_only) do
                selection bind(@setting, :direction)
              end
            end

            composite do
              layout_data :fill, :fill, true, false
              label do
                text 'Probability'
                font height: 16
              end

              composite do
                grid_layout(2, false) do
                  margin_width 0
                  margin_height 0
                end
                layout_data :fill, :center, true, false

                scale do
                  minimum 0
                  maximum 100
                  selection <=> [@setting, :probability]
                end

                label do
                  layout_data :right, :center, false, false
                  text <= [@setting, :probability, { on_read: ->(value) { "#{value} " } }]
                  font height: 16
                end
              end
            end

            composite do
              layout_data :fill, :fill, true, false
              label do
                text 'Colors'
                font height: 16
              end

              checkbox_group do
                selection bind(@setting, :colors)
              end
            end
          end
        end
      end
    end
  end
end

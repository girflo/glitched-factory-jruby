# frozen_string_literal: true

class GlitchedFactory
  module View
    class Glitcher
      # View for the change byte settings tab of the glitcher
      class ChangeByte
        include Glimmer::UI::CustomWidget

        # Settings for the change byte tab of the glitcher
        class Setting
          attr_accessor :byte_numbers

          def initialize
            self.byte_numbers = '0'
          end
        end

        def formatted_settings
          { byte_numbers: @setting.byte_numbers }
        end

        before_body do
          @setting = Setting.new
        end

        body do
          composite do
            label do
              text 'Byte numbers'
              font height: 16
            end

            text do
              text bind(@setting, :byte_numbers)
            end
          end
        end
      end
    end
  end
end

# frozen_string_literal: true

class GlitchedFactory
  module View
    class Glitcher
      # View for the exchange settings tab of the glitcher
      class Exchange
        include Glimmer::UI::CustomWidget

        # Settings for the exchange tab of the glitcher
        class Setting
          attr_accessor :filter, :filter_options, :random, :random_options, :range, :seed

          def initialize
            self.filter_options = %w[None Sub Up Average Paeth]
            self.filter = 'Average'
            self.random_options = %w[Yes No]
            self.random = 'No'
            self.range = '0'
            self.seed = 'x'
          end
        end

        def formatted_settings
          { filter: @setting.filter.downcase, random: Helper.string_to_bool(@setting.random),
            range: @setting.range.to_i, seed: @setting.seed }
        end

        before_body do
          @setting = Setting.new
        end

        body do
          composite do
            grid_layout(3, false) do
              margin_width 0
              margin_height 0
            end
            layout_data :fill, :fill, true, false

            composite do
              layout_data :fill, :fill, true, false
              label do
                text 'Filter'
                font height: 16
              end

              combo(:read_only) do
                selection bind(@setting, :filter) # also binds to country_options by convention
              end
            end

            composite do
              layout_data :fill, :fill, true, false
              label do
                text 'Random'
                font height: 16
              end

              combo(:read_only) do
                selection bind(@setting, :random) # also binds to country_options by convention
              end
            end

            composite do
              layout_data :fill, :fill, true, false
              label do
                text 'Range'
                font height: 16
              end

              text do
                text bind(@setting, :range)
              end
            end

            composite do
              layout_data :fill, :fill, true, false
              label do
                text 'Seed'
                font height: 16
              end

              text do
                text bind(@setting, :seed)
              end
            end
          end
        end
      end
    end
  end
end

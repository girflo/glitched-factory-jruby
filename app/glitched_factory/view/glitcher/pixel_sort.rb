# frozen_string_literal: true

class GlitchedFactory
  module View
    class Glitcher
      # View for the pixel sort settings tab of the glitcher
      class PixelSort
        include Glimmer::UI::CustomWidget

        # Settings for the pixel sort tab of the glitcher
        class Setting
          attr_accessor :direction, :direction_options, :smart_sorting, :smart_sorting_options, :detection_type,
                        :detection_type_options, :detection_min, :detection_max, :multiple_ranges,
                        :multiple_ranges_options, :detection_min_two, :detection_max_two, :sorting_by,
                        :sorting_by_options

          def initialize
            self.direction = 'vertical'
            self.direction_options = ['horizontal', 'horizontal inverted', 'vertical', 'vertical inverted']
            self.smart_sorting = 'Yes'
            self.smart_sorting_options = %w[Yes No]
            self.detection_type = 'lightness range'
            self.detection_type_options = ['lightness range', 'colors']
            self.detection_min = 45
            self.detection_max = 60
            self.multiple_ranges = 'Yes'
            self.multiple_ranges_options = %w[Yes No]
            self.detection_min_two = 75
            self.detection_max_two = 90
            self.sorting_by = 'hue'
            self.sorting_by_options = %w[hue saturation]
          end
        end

        def formatted_settings
          { direction: Helper.string_to_symbol(@setting.direction),
            smart_sorting: Helper.string_to_bool(@setting.smart_sorting),
            detection_type: Helper.string_to_symbol(@setting.detection_type), detection_min: @setting.detection_min.to_s,
            detection_max: @setting.detection_max.to_s, multiple_ranges: Helper.string_to_bool(@setting.multiple_ranges),
            detection_min_two: @setting.detection_min_two.to_s, detection_max_two: @setting.detection_max_two.to_s,
            sorting_by: Helper.string_to_symbol(@setting.sorting_by) }
        end

        before_body do
          @setting = Setting.new
        end

        body do
          composite do
            grid_layout(3, false) do
              margin_width 0
              margin_height 0
            end
            layout_data :fill, :fill, true, false

            composite do
              layout_data :fill, :fill, true, false
              label do
                text 'Direction'
                font height: 16
              end

              combo(:read_only) do
                selection bind(@setting, :direction)
              end
            end

            composite do
              layout_data :fill, :fill, true, false
              label do
                text 'Smart sorting'
                font height: 16
              end

              combo(:read_only) do
                selection bind(@setting, :smart_sorting)
              end
            end

            composite do
              layout_data :fill, :fill, true, false
              label do
                text 'Sorting by'
                font height: 16
              end

              combo(:read_only) do
                selection bind(@setting, :sorting_by)
              end
            end

            composite do
              layout_data :fill, :fill, true, false
              label do
                text 'Detection type'
                font height: 16
              end

              combo(:read_only) do
                selection bind(@setting, :detection_type)
              end
            end

            composite do
              layout_data :fill, :fill, true, false
              label do
                text 'Min'
                font height: 16
              end

              spinner do
                digits 0
                minimum 0
                maximum 10_000
                increment 1
                page_increment 10
                selection <=> [@setting, :detection_min]
              end
            end

            composite do
              layout_data :fill, :fill, true, false
              label do
                text 'Max'
                font height: 16
              end

              spinner do
                digits 0
                minimum 0
                maximum 10_000
                increment 1
                page_increment 10
                selection <=> [@setting, :detection_max]
              end
            end

            composite do
              layout_data :fill, :fill, true, false
              label do
                text 'Multiple ranges'
                font height: 16
              end

              combo(:read_only) do
                selection bind(@setting, :multiple_ranges)
              end
            end

            composite do
              layout_data :fill, :fill, true, false
              label do
                text 'Second min'
                font height: 16
              end

              spinner do
                digits 0
                minimum 0
                maximum 10_000
                increment 1
                page_increment 10
                selection <=> [@setting, :detection_min_two]
              end
            end

            composite do
              layout_data :fill, :fill, true, false
              label do
                text 'Second max'
                font height: 16
              end

              spinner do
                digits 0
                minimum 0
                maximum 10_000
                increment 1
                page_increment 10
                selection <=> [@setting, :detection_max_two]
              end
            end
          end
        end
      end
    end
  end
end

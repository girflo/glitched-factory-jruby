# frozen_string_literal: true

class GlitchedFactory
  module View
    class Glitcher
      # View for the defect settings tab of the glitcher
      class Defect
        include Glimmer::UI::CustomWidget

        # Settings for the defect tab of the glitcher
        class Setting
          attr_accessor :random, :random_options, :iterations

          def initialize
            self.random_options = %w[Yes No]
            self.random = 'No'
            self.iterations = 1
          end
        end

        def formatted_settings
          { random: Helper.string_to_bool(@setting.random), iterations: @setting.iterations }
        end

        before_body do
          @setting = Setting.new
        end

        body do
          composite do
            grid_layout(3, false) do
              margin_width 0
              margin_height 0
            end
            layout_data :fill, :fill, true, false

            composite do
              layout_data :fill, :fill, true, false
              label do
                text 'Random'
                font height: 16
              end

              combo(:read_only) do
                selection bind(@setting, :random) # also binds to country_options by convention
              end
            end

            composite do
              layout_data :fill, :fill, true, false
              label do
                text 'Iterations'
                font height: 16
              end

              text do
                text bind(@setting, :iterations)
              end
            end
          end
        end
      end
    end
  end
end

# frozen_string_literal: true

class GlitchedFactory
  module View
    # View for the glitcher tab
    class Glitcher
      include Glimmer::UI::CustomWidget

      body do
        @io = io_fields
        @glitcher = ''
        @conveyor = GlitchedFactory::Queue.conveyor

        composite do
          layout_data :fill, :fill, true, true

          tab_folder do
            layout_data :fill, :fill, true, true

            tab_item do
              text 'Brush'
              @brush = brush
              button do
                text 'Start brush'
                on_widget_selected do
                  @conveyor.add_parcel('Brush', '',
                                       ShatteredMachine::Glitcher.new('Brush', @io.object, @brush.formatted_settings).call)
                end
              end
            end
            tab_item do
              text 'Change byte'
              @change_byte = change_byte
              button do
                text 'Start change byte'
                on_widget_selected do
                  @conveyor.add_parcel('Change byte', '',
                                       ShatteredMachine::Glitcher.new('ChangeByte', @io.object, @change_byte.formatted_settings).call)
                end
              end
            end
            tab_item do
              text 'Defect'
              @defect = defect
              button do
                text 'Start defect'
                on_widget_selected do
                  @conveyor.add_parcel('Defect', '',
                                       ShatteredMachine::Glitcher.new('Defect', @io.object, @defect.formatted_settings).call)
                end
              end
            end
            tab_item do
              text 'Exchange'
              @exchange = exchange
              button do
                text 'Start exchange'
                on_widget_selected do
                  @conveyor.add_parcel('Exchange', '',
                                       ShatteredMachine::Glitcher.new('Exchange', @io.object, @exchange.formatted_settings).call)
                end
              end
            end
            tab_item do
              text 'Pixel Sorter'
              @pixel_sort = pixel_sort
              button do
                text 'Start pixel sort'
                on_widget_selected do
                  @conveyor.add_parcel('Pixel sorter', '',
                                       ShatteredMachine::Glitcher.new('PixelSorter', @io.object, @pixel_sort.formatted_settings).call)
                end
              end
            end
            tab_item do
              text 'Slim'
              @slim = slim
              button do
                text 'Start slim'
                on_widget_selected do
                  @conveyor.add_parcel('Slim', '',
                                       ShatteredMachine::Glitcher.new('Slim', @io.object, @slim.formatted_settings).call)
                end
              end
            end
            tab_item do
              text 'Transpose'
              @transpose = transpose
              button do
                text 'Start transpose'
                on_widget_selected do
                  @conveyor.add_parcel('Transpose', '',
                                       ShatteredMachine::Glitcher.new('Transpose', @io.object, @transpose.formatted_settings).call)
                end
              end
            end
            tab_item do
              text 'Wrong filter'
              @wrong_filter = wrong_filter
              button do
                text 'Start wrong filter'
                on_widget_selected do
                  @conveyor.add_parcel('Wrong filter', '',
                                       ShatteredMachine::Glitcher.new('WrongFilter', @io.object, @wrong_filter.formatted_settings).call)
                end
              end
            end
          end
        end
      end
    end
  end
end

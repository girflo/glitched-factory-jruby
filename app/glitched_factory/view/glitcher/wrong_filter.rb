# frozen_string_literal: true

class GlitchedFactory
  module View
    class Glitcher
      # View for the wrong filter settings tab of the glitcher
      class WrongFilter
        include Glimmer::UI::CustomWidget

        # Settings for the wrong filter tab of the glitcher
        class Setting
          attr_accessor :filter, :filter_options

          def initialize
            self.filter_options = %w[None Sub Up Average Paeth]
            self.filter = 'Average'
          end
        end

        def formatted_settings
          { filter: @setting.filter.downcase }
        end

        before_body do
          @setting = Setting.new
        end

        body do
          composite do
            layout_data :fill, :fill, true, false

            label do
              text 'Filter'
              font height: 16
            end

            combo(:read_only) do
              selection bind(@setting, :filter) # also binds to country_options by convention
            end
          end
        end
      end
    end
  end
end

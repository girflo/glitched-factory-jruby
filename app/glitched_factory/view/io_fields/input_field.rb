# frozen_string_literal: true

class GlitchedFactory
  module View
    class IoFields
      # View for the io input field
      class InputField
        include Glimmer::UI::CustomWidget
        attr_accessor :source

        before_body do
          self.source = CONTENT['io']['none']
        end

        body do
          composite do
            grid_layout(3, false) do
              margin_width 2
              margin_height 2
            end
            layout_data :fill, :center, true, false

            button do
              text CONTENT['io']['directory']

              on_widget_selected do
                self.source = directory_dialog.open
              end
            end

            label do
              text CONTENT['io']['or']
              font height: 16
            end

            button do
              text CONTENT['io']['single_image']

              on_widget_selected do
                self.source = file_dialog.open
              end
            end
          end

          composite do
            label do
              layout_data :right, :center, false, false
              text CONTENT['io']['selected_input']
              font height: 16
            end

            label do
              layout_data :fill, :center, true, false
              text bind(self, :source)
              font height: 16
            end
          end
        end
      end
    end
  end
end

# frozen_string_literal: true

class GlitchedFactory
  module View
    class IoFields
      # View for the io input field using only files
      class InputFieldSingle
        include Glimmer::UI::CustomWidget
        attr_accessor :source

        before_body do
          self.source = CONTENT['io']['none']
        end

        body do
          composite do
            grid_layout(2, false) do
              margin_width 2
              margin_height 2
            end
            layout_data :fill, :center, true, false

            button do
              layout_data :right, :center, false, false
              text CONTENT['io']['image']

              on_widget_selected do
                self.source = file_dialog.open
              end
            end
          end

          composite do
            label do
              layout_data :right, :center, false, false
              text CONTENT['io']['selected_input']
              font height: 16
            end

            label do
              layout_data :fill, :center, true, false
              text bind(self, :source)
              font height: 16
            end
          end
        end
      end
    end
  end
end

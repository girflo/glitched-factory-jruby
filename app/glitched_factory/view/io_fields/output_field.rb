# frozen_string_literal: true

class GlitchedFactory
  module View
    class IoFields
      # View for the io output field
      class OutputField
        include Glimmer::UI::CustomWidget
        attr_accessor :folder, :filename

        before_body do
          self.folder = CONTENT['io']['none']
          self.filename = CONTENT['io']['default_filename']
        end

        body do
          button do
            text CONTENT['io']['directory']
            on_widget_selected do
              self.folder = directory_dialog.open
            end
          end

          composite do
            label do
              layout_data :right, :center, false, false
              text CONTENT['io']['selected_folder']
              font height: 16
            end

            label do
              layout_data :fill, :center, true, false
              text bind(self, :folder)
              font height: 16
            end
          end

          composite do
            label do
              layout_data :right, :center, false, false
              text CONTENT['io']['output_name_schema']
              font height: 16
            end

            composite do
              grid_layout(2, false) do
                margin_width 2
                margin_height 2
              end
              layout_data :fill, :center, true, false

              text do
                layout_data :fill, :center, true, false
                text bind(self, :filename)
              end

              label do
                layout_data :right, :center, false, false
                text CONTENT['io']['extension']
                font height: 16
              end
            end
          end
        end
      end
    end
  end
end

# frozen_string_literal: true

class GlitchedFactory
  module View
    # View for the io fields using input only for files
    class IoFieldsSingle
      include Glimmer::UI::CustomWidget
      def object
        ShatteredMachine::Io.new(@input.source,
                                 @output.folder, @output.filename)
      end

      body do
        composite do
          grid_layout 2, true
          layout_data :fill, :fill, true, false

          group do
            text CONTENT['io']['input']
            font height: 16, style: :bold
            layout_data :fill, :fill, true, true
            @input = input_field_single
          end

          group do
            text CONTENT['io']['output']
            font height: 16, style: :bold
            layout_data :fill, :fill, true, true
            @output = output_field
          end
        end
      end
    end
  end
end

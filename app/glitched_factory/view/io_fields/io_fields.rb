# frozen_string_literal: true

class GlitchedFactory
  module View
    # View for the io fields
    class IoFields
      include Glimmer::UI::CustomWidget
      def object
        ShatteredMachine::Io.new(@input.source,
                                 @output.folder, @output.filename)
      end

      body do
        composite do
          grid_layout 2, true
          layout_data :fill, :fill, true, false

          group do
            layout_data :fill, :fill, true, false
            text CONTENT['io']['input']
            font height: 16, style: :bold
            @input = input_field
          end

          group do
            layout_data :fill, :fill, true, false
            text CONTENT['io']['Output']
            font height: 16, style: :bold
            @output = output_field
          end
        end
      end
    end
  end
end

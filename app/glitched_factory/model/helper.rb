# frozen_string_literal: true

class GlitchedFactory
  # Contains methods helping formatting text
  class Helper
    def self.string_to_symbol(string)
      string.gsub(/\s+/, '_').downcase.to_sym
    end

    def self.string_to_bool(string)
      string == 'Yes'
    end
  end
end

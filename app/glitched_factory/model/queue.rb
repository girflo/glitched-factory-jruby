# frozen_string_literal: true

class GlitchedFactory
  # Contains the logic related to the queue, it's bassically a wrapper for TinyConveyor
  class Queue
    def self.conveyor
      @conveyor ||= TinyConveyor.new
    end
  end
end
